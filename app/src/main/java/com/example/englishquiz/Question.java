package com.example.englishquiz;

import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Parcelable {
    String question;
    String option1;
    String option2;
    String option3;
    String option4;
    Question(){}
    private int answerNumber;

    public String getQuestion() {
        return question;
    }

    public String getOption1() {
        return option1;
    }

    public String getOption2() {
        return option2;
    }

    public String getOption3() {
        return option3;
    }

    public String getOption4() { return option4;  }

    public int getAnswerNumber() {
        return answerNumber;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public void setAnswerNumber(int answerNumber) {
        this.answerNumber = answerNumber;
    }



    public Question(String question, String option1, String option2, String option3, String option4, int answerNumber) {
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.answerNumber = answerNumber;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(option1);
        dest.writeString(option2);
        dest.writeString(option3);
        dest.writeInt(answerNumber);
    }
}
