package com.example.englishquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final int Request_code = 1;
    public static final String SHARED_PREFS = "sharedPrefs";
    private static final String KEY_HIGH_SCORE = "key_Highscore";


    public TextView textViewHighScore;
    private int highscore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);
        Button buttonStartQuiz = findViewById(R.id.button_start_quiz);
        textViewHighScore =(TextView) findViewById(R.id.text_view_highscore);
        loadHighscore();

        buttonStartQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startQuiz();
            }
        });
    }

    private void startQuiz() {
        Intent intent = new Intent(MainActivity.this, InterrogationActivity.class);
        startActivityForResult(intent,Request_code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Request_code)
        {
            if(resultCode==RESULT_OK)
            {
                int score  = data.getIntExtra(InterrogationActivity.EXTRA_SCORE,0);
                if(score>highscore)
                {
                    updateHighScore(score);
                }
            }

        }
    }
    private void loadHighscore() {
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        highscore = prefs.getInt(KEY_HIGH_SCORE, 0);
         textViewHighScore.setText("Highscore: " + highscore);
    }

    private void updateHighScore(int highscoreNew){
        highscore=highscoreNew;
        textViewHighScore.setText("HighScore" + highscore);

    SharedPreferences prefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putInt(KEY_HIGH_SCORE, highscore);
    editor.apply();
}

}