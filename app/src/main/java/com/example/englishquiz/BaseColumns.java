package com.example.englishquiz;

public interface BaseColumns {
    public static final String _ID = "_id";
    public static final String _COUNT = "_count";
}
